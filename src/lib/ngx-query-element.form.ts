export const QUERY_ELEMENT_FORM = {
  "settings": {
    "i18n": {
      "el": {
      },
      "en": {
      }
    }
  },
  "components": [
    {
      "label": "Columns",
      "columns": [
        {
          "components": [

            {
              "label": "Select",
              "widget": "choicesjs",
              "hideLabel": true,
              "tableView": true,
              "dataSrc": "custom",
              "data": {
                "custom": "values = form.properties"
              },
              "customOptions": {
                "shouldSort": true,
                "searchResultLimit": 15,
                "removeItemButton": false,
              },
              "dataType": "object",
              "key": "left",
              "type": "select",
              "input": true
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0,
          "size": "md",
          "currentWidth": 3
        },
        {
          "components": [
            {
              "label": "Select",
              "widget": "choicesjs",
              "hideLabel": true,
              "tableView": true,
              "customOptions": {
                "searchResultLimit": 15,
                "removeItemButton": false,
              },
              "data": {
                "values": [
                  {
                    "label": "equal",
                    "value": "$eq"
                  },
                  {
                    "label": "notEqual",
                    "value": "$ne"
                  },
                  {
                    "label": "greaterThan",
                    "value": "$gt"
                  },
                  {
                    "label": "greaterOrEqual",
                    "value": "$ge"
                  },
                  {
                    "label": "lowerThan",
                    "value": "$lt"
                  },
                  {
                    "label": "lowerOrEqual",
                    "value": "$le"
                  },
                  {
                    "label": "contains",
                    "value": "$contains"
                  },
                  {
                    "label": "equalNull",
                    "value": "$eq null"
                  }
                ]
              },
              "key": "operator",
              "type": "select",
              "input": true
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0,
          "size": "md",
          "currentWidth": 3
        },
        {
          "label": "RightColumn",
          "columns": [

          ],
          "key": "rightColumn",
          "type": "columns",
          "input": false,
          "tableView": false
        },
        {
          "label": "Right",
          "components": [
            {
              "label": "Edm.None",
              "hideLabel": true,
              "tableView": true,
              "disabled": true,
              "key": "right",
              "type": "textfield",
              "calculateValue": "value = (data.left == null || (data.left && data.left.value == null) || data.operator == '$eq null') ? '': value",
              "redrawOn": "operator",
              "customConditional": "show = (data.left == null || (data.left && data.left.value == null) || data.operator == '$eq null')",
              "input": true,
            },
            {
              "label": "Edm.String",
              "hideLabel": true,
              "tableView": true,
              "key": "right",
              "type": "textfield",
              "customConditional": "show = (data.left && (data.left.type == 'Note' || data.left.type == 'Edm.String' || data.left.type == 'Edm.Guid') && data.operator != '$eq null')",
              "input": true
            },
            {
              "label": "Edm.Number",
              "hideLabel": true,
              "tableView": true,
              "key": "right",
              "type": "number",
              "customConditional": "show = !!(data.left && (data.left.type === 'Edm.Int16' || data.left.type === 'Edm.Int32' || data.left.type === 'Edm.Int64' || data.left.type === 'Edm.Decimal' || data.left.type === 'Edm.Double') && data.operator != '$eq null')",
              "input": true,
            },
            {
              "label": "Edm.DateTimeOffset",
              "customProperties": {
                "target": "right"
              },
              "useLocaleSettings": true,
              "format": "dd-MMM-yyyy",
              "hideLabel": true,
              "hidden": true,
              "tableView": false,
              "customConditional": "show = !!(data.left && (data.left.type == 'Edm.DateTimeOffset' || data.left.type == 'Edm.Date') && data.operator != '$eq null')",
              "datePicker": {
                "disableWeekends": false,
                "disableWeekdays": false
              },
              "enableTime": false,
              "enableMinDateInput": false,
              "enableMaxDateInput": false,
              "key": "right",
              "type": "datetime",
              "input": true,
              "widget": {
                "type": "calendar",
                "displayInTimezone": "viewer",
                "locale": "en",
                "useLocaleSettings": true,
                "allowInput": true,
                "mode": "single",
                "enableTime": false,
                "noCalendar": false,
                "format": "dd-MMM-yyyy",
                "hourIncrement": 1,
                "minuteIncrement": 1,
                "time_24hr": false,
                "minDate": null,
                "disableWeekends": false,
                "disableWeekdays": false,
                "maxDate": null
              }
            },
            {
              "label": "Edm.Collection",
              "customProperties": {
                "target": "right"
              },
              "customOptions": {
                "placeholder": true,
                "placeholderValue": "Empty"
              },
              "labelPosition": "top",
              "widget": "choicesjs",
              "hideLabel": true,
              "searchEnabled": false,
              "dataSrc": "url",
              "customConditional": "show = !!(data.left && (data.left.type == 'Edm.Collection') && (data.operator != '$eq null'))",
              "data": {
                "values": [],
                "url": "/{{data.left.entitySet}}?$select=id as value,name as text"
              },
              "selectThreshold": 0.3,
              "lazyLoad": true,
              "template": "{{ item.text }}",
              "dataType": "object",
              "idPath": "value",
              "selectValues": "value",
              "key": "right",
              "type": "select",
              "input": true
            }
          ],
          "size": "md",
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0,
          "currentWidth": 3
        },
        {
          "components": [
            {
              "label": "",
              "action": "event",
              "showValidations": false,
              "theme": "gray-100",
              "customClass": "text-theme pr-2 d-none d-md-block",
              "leftIcon": "fa fa-times",
              "tableView": false,
              "key": "remove",
              "type": "button",
              "event": "remove",
              "input": true,
              "logic": [
                {
                  "name": "disableRemoveLogic",
                  "trigger": {
                    "type": "simple",
                    "simple": {
                      "show": true,
                      "when": "removable",
                      "eq": false
                    }
                  },
                  "actions": [
                    {
                      "name": "disableRemoveLogicAction",
                      "type": "property",
                      "property": {
                        "label": "Disabled",
                        "value": "disabled",
                        "type": "boolean"
                      },
                      "state": true
                    }
                  ]
                }
              ]
            },
            {
              "label": "RemoveFilter",
              "action": "event",
              "showValidations": false,
              "theme": "gray-100",
              "block": true,
              "customClass": "text-theme btn-sm d-md-none d-xs-block",
              "tableView": false,
              "key": "remove",
              "type": "button",
              "event": "remove",
              "input": true,
              "logic": [
                {
                  "name": "disableRemoveLogic",
                  "trigger": {
                    "type": "simple",
                    "simple": {
                      "show": true,
                      "when": "removable",
                      "eq": false
                    }
                  },
                  "actions": [
                    {
                      "name": "disableRemoveLogicAction",
                      "type": "property",
                      "property": {
                        "label": "Disabled",
                        "value": "disabled",
                        "type": "boolean"
                      },
                      "state": true
                    }
                  ]
                }
              ]
            }
          ],
          "size": "md",
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0,
          "currentWidth": 3
        }
      ],
      "key": "columns",
      "type": "columns",
      "input": false,
      "tableView": false
    }
  ]
}