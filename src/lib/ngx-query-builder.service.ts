import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { QUERY_ELEMENT_FORM } from './ngx-query-element.form';
import { cloneDeep } from 'lodash';
import { ServiceUrlPreProcessor } from '@universis/forms';
import { ComponentSchema } from 'formiojs';
import { TranslateService } from '@ngx-translate/core';
import { EventEmitter } from '@angular/core';
import { EdmSchema, TextUtils } from '@themost/client';

export declare interface FormComponent extends ComponentSchema {
  components?: FormComponent[];
  properties?: {
    entityType?: string,
    entitySet?: string,
  },
  columns?: FormComponent[];
  customConditional?: string;
  dataSrc?: string;
  data?: any;
  selectThreshold?: number;
  lazyLoad?: boolean;
  template?: string;
  valueProperty?: string;
  selectValues?: string;
  dataType?: string;
  idPath?: string
}

export interface QueryElement {
  operator: string;
  type?: string;
  elements?: QueryElement[];
}

export interface QueryProperty {
  label: string,
  value: string,
  type: string,
  entitySet?: string,
  hidden?: boolean,
  source?: string,
  template?: string
}

export interface BasicQueryElement extends QueryElement {
  left: any;
  right: any;
}

export interface CompositeQueryElement extends QueryElement  {
  elements: (BasicQueryElement | CompositeQueryElement)[];
}

@Injectable()
export class QueryBuilderService {

  public loadingComponent = new EventEmitter<FormComponent>(false);
  public loadingProperties = new EventEmitter<QueryProperty[]>(false);

  constructor(protected context: AngularDataContext, protected translateService: TranslateService) { }

  public findComponent(element: FormComponent, label: string) {
    if (Array.isArray(element.components)) {
      for (const item of element.components) {
        if (item.label === label) {
          return item;
        }
        const result = this.findComponent(item, label);
        if (result) {
          return result;
        }
      }
    }
    if (Array.isArray(element.columns)) {
      for (const item of element.columns) {
        if (item.label === label) {
          return item;
        }
        const result = this.findComponent(item, label);
        if (result) {
          return result;
        }
      }
    }
  }

  private deepFindProperty(schema: EdmSchema, entityType: string, property: string): { Name: string, Type: string, EntityType: string } | undefined {
    let thisEntityType = schema.EntityType.find((item) => item.Name === entityType);
    if (thisEntityType != null) {
      // try to find property
      let find: { Name: string, Type: string } | undefined = thisEntityType.Property.find((item) => item.Name === property);
      if (find != null) {
        return {
          Name: find.Name,
          Type: find.Type,
          EntityType: thisEntityType.Name
        };
      }
      // try to find navigation property
      find = thisEntityType.NavigationProperty.find((item) => item.Name === property);
      if (find != null) {
        return {
          Name: find.Name,
          Type: find.Type,
          EntityType: thisEntityType.Name
        };
      }
      if (thisEntityType.BaseType) {
        return this.deepFindProperty(schema, thisEntityType.BaseType, property);
      }
    }
    return;
  }

  private deepFindAnyProperty(schema: EdmSchema, anyEntityType: string, anyProperty: string): { Name: string, FullyQualifiedName?: string, Type: string, EntityType: string } | undefined {
    const properties = anyProperty.split('/');
    let entityType = anyEntityType;
    let index = 0;
    let result: { Name: string; Type: string; EntityType: string; } | undefined;
    while(index < properties.length) {
      const property = properties[index];
      result = this.deepFindProperty(schema, entityType, property);
      if (index < properties.length) {
        if (result && result.Type) {
          entityType = result.Type;
        } 
      }
      index += 1;
    }
    if (result) {
      return Object.assign(result, {
        FullyQualifiedName: anyProperty
      });
    }
    return;
  }

  async getQueryElementForm(entityType: string, extraOptions?: {
    includeProperties: string[]
  }) {
    const schema = await this.context.getMetadata();
    const findEntityType = schema.EntityType.find((item) => item.Name === entityType);
    if (findEntityType == null) {
      throw new Error('The specified entity type cannot be found');
    }
    let props: { Name: string, FullyQualifiedName?: string, Type: string, EntityType: string }[] = [];
    if (extraOptions && extraOptions.includeProperties) {
      extraOptions.includeProperties.forEach((includeProperty) => {
        const prop = this.deepFindAnyProperty(schema, entityType, includeProperty);
        if (prop) {
          props.push(prop)
        }
      });
    } else {
      props = ([] as any[]).concat(...findEntityType.Property).concat(...findEntityType.NavigationProperty);
    }
    
    const properties : QueryProperty[] =  props.map((prop) => {
      const result: QueryProperty = {
          label: prop.Name,
          value: `$${prop.FullyQualifiedName}`,
          type: prop.Type,
        };
        if (/^Edm\./.test(prop.Type) === false) {
          const findEntitySet = schema.EntityContainer.EntitySet.find((item) => item.EntityType === prop.Type);
          if (findEntitySet) {
            Object.assign(result, {
              entitySet: findEntitySet.Name
            });
            const limit = -1;
            Object.defineProperty(result, 'source', {
              configurable: true,
              enumerable: false,
              writable: true,
              value: `/${result.entitySet}?$top=${{ limit }}&$skip={{ skip }}&$orderby=name&$select=id as raw,name as label`
            });
          }
        }
        return result;
      }).sort((a, b) => {
        if (a.label > b.label) {
          return 1;
        }
        if (a.label < b.label) {
          return -1;
        }
        return 0;
      });
    
    const newQueryElementForm = cloneDeep(QUERY_ELEMENT_FORM);
    // get template component
    const rightComponent: FormComponent = this.findComponent(newQueryElementForm as any, 'Right');
    this.loadingProperties.emit(properties);
    properties.forEach((item) => {
      if (item.entitySet != null) {
        // try to find if component has been already set
        const findComponent = this.findComponent(rightComponent, item.type);
        if (findComponent == null) {
          // create component for entitySet
          const newComponent = {
            "label": item.type,
            "properties": {
              "entityType": item.type,
              "entitySet": item.entitySet
            },
            "widget": "choicesjs",
            "hideLabel": true,
            "dataSrc": "url",
            "customOptions": {
              "placeholder": true,
              "placeholderValue": this.translateService.instant('QueryBuilder.Empty')
            },
            "customConditional": `show = !!(data.left && (data.left.type === '${item.type}') && (data.operator != '$eq null'))`,
            "data": {
              "url": item.source
            },
            "selectThreshold": 0.3,
            "lazyLoad": true,
            "template": item.template || "{{ item.label }}",
            "dataType": "object",
						"idPath": "val",
            "selectValues": "value",
            "key": "right",
            "type": "select",
            "input": true
          };
          this.loadingComponent.emit(newComponent);
          if (Array.isArray(rightComponent.components)) {
            rightComponent.components.push(newComponent);
          }
        }
      }
    });
    new ServiceUrlPreProcessor(this.context).parse(newQueryElementForm);
    Object.assign(newQueryElementForm, {
      properties: properties.filter((property) => !property.hidden)
    });
    return newQueryElementForm;
  }

  public getQueryFrom(element: QueryElement): string {
    if (element.type === 'LogicalExpression') {
      // expect element.elements to be an instance of Array
      if (Array.isArray(element.elements) === false) {
        throw new Error('A logical expression should have two or more expressions');
      }
      const results: string[] = [];
      if (Array.isArray(element.elements)) {
        for (const child of element.elements) {
          const result = this.getQueryFrom(child);
          results.push(result);
        }
      }
      let str = results.length > 1 ? '(' : '';
      str += results.join(' ' + element.operator.replace(/^\$/, '') + ' ');
      str += results.length > 1 ? ')' : '';;
      return str;
    } else if (element.type === 'BinaryExpression') {
      // expect left and right operands
      const leftOperand = (element as BasicQueryElement).left;
      if (leftOperand == null) {
        throw new Error('Expected left operand');
      }
      const right = (element as BasicQueryElement).right;
      if (right == null && element.operator && element.operator != '$eq null') {
        throw new Error('Expected right operand');
      }
      const left = leftOperand.value.replace(/^\$/, '').replace(/^\./g, '/');
      const operator = element.operator.replace(/^\$/, '');
      let value: any;
      if (typeof right === 'object') {
        value = TextUtils.escape(right.raw);
      } else {
        value = TextUtils.escape(right);
      }
      if (operator === 'contains') {
        return `(indexof(${left}, ${value}) ge 0)`;
      }
      if (operator === 'eq null') {
        return `(${left} ${operator})`; 
      }
      return `(${left} ${operator} ${value})`;
    } else {
      throw new Error(`Query element of type ${element.type} is not supported`);
    }
  }

  public getTextFrom(element: QueryElement): string {
    if (element.type === 'LogicalExpression') {
      // expect element.elements to be an instance of Array
      if (Array.isArray(element.elements) === false) {
        throw new Error('A logical expression should have two or more expressions');
      }
      const results: string[] = [];
      if (Array.isArray(element.elements)) {
        for (const child of element.elements) {
          const result = this.getTextFrom(child);
          results.push(result);
        }
      }
      let str = results.length > 1 ? '(' : '';
      str += results.join(' ' + this.translateService.instant(`QueryBuilder.${element.operator}`) + ' ');
      str += results.length > 1 ? ')' : '';;
      return str;
    } else if (element.type === 'BinaryExpression') {
      // expect left and right operands
      const leftOperand = (element as BasicQueryElement).left;
      if (leftOperand == null) {
        throw new Error('Expected left operand');
      }
      const right = (element as BasicQueryElement).right;
      if (right == null && element.operator && element.operator != '$eq null') {
        throw new Error('Expected right operand');
      }
      const left = this.translateService.instant(leftOperand.label);
      const operator = this.translateService.instant(`QueryBuilder.${element.operator}`).replace(/^\./g, '/');
      let value: any;
      if (typeof right === 'object') {
        value = this.translateService.instant(right.label);
      } else {
        value = (right == null || right === '') ? this.translateService.instant('QueryBuilder.Empty') : right;
      }
      return `(${left} ${operator} ${value})`;
    } else {
      throw new Error(`Query element of type ${element.type} is not supported`);
    }
  }



}
