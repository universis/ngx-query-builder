import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { CompositeQueryElement, QueryBuilderService, FormComponent, QueryProperty } from './ngx-query-builder.service';
import { EdmEntityType } from '@themost/client';
import { TranslateService } from '@ngx-translate/core';
import { EventEmitter } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'universis-query-builder',
  styleUrls: [
    './ngx-query-builder.scss'
  ],
  templateUrl: './ngx-query-builder.component.html',
  animations: [
    trigger(
      'inOutAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ opacity: 0 }),
            animate('0.5s ease-out', 
                    style({ opacity: 1 }))
          ]
        ),
        transition(
          ':leave', 
          [
            style({ opacity: 1 }),
            animate('0.25s ease-in', 
                    style({ opacity: 0 }))
          ]
        )
      ]
    )
  ],
  encapsulation: ViewEncapsulation.None
})
export class QueryBuilderComponent implements OnInit, AfterViewInit, OnChanges {

  public lastError?: Error;
  @ViewChild('builder') builder?: ElementRef;

  @Input() @Output() query?: CompositeQueryElement;
  @Input() persistent?: boolean = false;
  @Input() includeProperties: string[] = [];
  @Input() customParams: any = {
  };
  public saving = false;
  public elementTemplate?: EdmEntityType;
  @Input() entityType?: string;
  public currentLang: string = 'en';
  @Output() loadingComponent = new EventEmitter<FormComponent>(false);
  @Output() loadingProperties = new EventEmitter<QueryProperty[]>(false);
  @Output() save = new EventEmitter<{
    title: string,
    entityType: string,
    query: CompositeQueryElement
  }>();
  
  constructor(protected context: AngularDataContext,
    protected translateService: TranslateService,
    protected queryBuilder: QueryBuilderService) { }
  
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.entityType) {
      //
    }
  }

  ngAfterViewInit(): void {
    this.lastError = undefined;
    // get metadata
    const propertySubscription = this.queryBuilder.loadingProperties.subscribe((properties) => {
      this.loadingProperties.emit(properties);
    });
    const componentSubscription = this.queryBuilder.loadingComponent.subscribe((formComponent) => {
      this.loadingComponent.emit(formComponent)
    });
    if (this.entityType) {
      this.queryBuilder.getQueryElementForm(this.entityType, {
        includeProperties: this.includeProperties
      }).then((template) => {
        this.elementTemplate = template as any;
        componentSubscription.unsubscribe();
        propertySubscription.unsubscribe();
      });
    }
  }

  ngOnInit(): void {
    this.currentLang = this.translateService.currentLang;
  }

  addFilter() {
    const current = JSON.parse(JSON.stringify(this.query));
    setTimeout(() => {
      this.query = {
        type: 'LogicalExpression',
        operator: '$and',
        elements: [
          current,
          {
            operator: '$eq',
            left: null,
            right: null
          }
        ]
      }
    }, 50);
  }

  switchGroup(operator: '$and' | '$or') {
    if (this.query) {
      this.query.operator = operator;
    }
  }

  @Input()
  get queryParams(): any {
    if (this.query == null) {
      return {}
    }
     return {
        $filter: this.queryBuilder.getQueryFrom(this.query)
     }
  }

  @Input() get queryDescription(): string | undefined {
    if (this.query == null) {
      return;
    }
    return this.queryBuilder.getTextFrom(this.query);
  }

  getQueryDescription(): string | undefined {
    if (this.query == null) {
      return;
    }
    return this.queryBuilder.getTextFrom(this.query);
  }

  onSave(formComponent: NgForm) {
    this.save.emit(Object.assign(formComponent.form.value, {
      query: this.query,
      entityType: this.entityType
    }));
  }
  onCancelSave() {
    this.saving = false;
  }

}
